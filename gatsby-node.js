const path = require(`path`);
const makeRequest = (graphql, request) => new Promise((resolve, reject) => {
    // Запрос для получения данных, используемых при создании страниц.
    resolve(
        graphql(request).then(result => {
            if (result.errors) {
                reject(result.errors)
            }
            return result;
        })
    )
});

exports.createPages  = ({graphql, actions}) => {
    const {createPage} = actions;
    const blogPostTemplate = path.resolve(`src/templates/article.js`);

    const getArticles = makeRequest(graphql, `
           query Articles {
                  allArticleJson(limit: 1000) {
                    edges {
                      node {
                        url
                      }
                    }
                  }
                }

    `).then(result => {
        // Создаём страницы для каждой статьи.


        result.data.allArticleJson.edges.forEach((document) => {
            createPage({
                path: `/article/${document.node.url}`,
                component: blogPostTemplate,
                context: {
                    url: document.node.url,
                },
            })
        })
    });

    // Запросы материалов статей и данных авторов для использования при создании страниц.
    return Promise.all([
        getArticles
    ])
};
