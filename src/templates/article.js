import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"

const ArticleTemplate = ({data}) => (
    <Layout>
        {console.log(data)}
        <SEO title={data.articleJson.name} />
        <div>
            <h1>{data.articleJson.name}</h1>
            {data.articleJson.description}
        </div>
    </Layout>
)
export default ArticleTemplate
export const query = graphql`
query Article($url: String!)  {
  articleJson(url: {eq: $url}) {
    id
    name
    description
  }
}
`
