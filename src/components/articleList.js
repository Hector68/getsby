import React from 'react';
import Article from "./article";
import styles from './article.module.css';
import {graphql, StaticQuery} from "gatsby";

const ArticleList = () => {

    return <StaticQuery query={graphql`  
                            query Artice {
                              allArticleJson (sort: { fields:[id], order: DESC} ){
                                edges {
                                  node {
                                    id,
                                    url,
                                    name,
                                    short_description
                                  }
                                }
                              }
                            }
                            `}
                        render={data => (
                            <ul className={styles.ul}>
                                {data.allArticleJson.edges.map(document => (
                                    <Article key={document.node.id} document={document.node}/>
                                ))}
                            </ul>
                        )}
    />

};

export default ArticleList;

