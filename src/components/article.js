import React, {Component} from 'react';
import {Link} from "gatsby";
import styles from './article.module.css';


class Article extends Component {

    state = {
        isOpen: true
    };

    handleClick = () => {
        this.setState(
            {
                isOpen: !this.state.isOpen
            }
        );
    };

    render() {
        const {document} = this.props;
        return <li className={styles.item}>
            <h2>
                <font color="#3AC1EF">
                    <Link to={`/article/${document.url}`}>{document.name}</Link>
                    <button className={styles.button} onClick={this.handleClick}>
                        {this.state.isOpen ? 'Close' : 'Open'}
                    </button>
                </font>
            </h2>
            {this.state.isOpen && <p>{document.short_description}</p>}
        </li>;
    }
}

export default Article;
